/*
  Web Server
 
 A simple web server
 using an Arduino Wiznet Ethernet shield. 
 
 Circuit:
 * Ethernet shield attached to pins 10, 11, 12, 13
 * Analog inputs attached to pins A0 through A5 (optional)
 
 created 18 Dec 2009
 by David A. Mellis
 modified 4 Sep 2010
 by Tom Igoe
 modified 31 Dec 2017
 by Baptiste Jammet
 */

#include <SPI.h>
#include <Ethernet.h>

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
// byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
byte mac[] = { 0x90, 0xA2, 0xDA, 0x0D, 0x01, 0xB8 }; // printed on the sticker
IPAddress ip(192,168,1, 7);

// Initialize the Ethernet server library
// with the IP address and port you want to use 
// (port 80 is default for HTTP):
EthernetServer server(80);

// Digital Outputs
int relayPin = 7;

String simpleHash(String inputString) {
  String outputString = inputString;
  for(unsigned int i=0; i<inputString.length() ; i++) {
    int tempChar = (inputString[i] * inputString[i]) + (i * i);
    while (tempChar > 126) {
      tempChar = tempChar - 94;
    }
    outputString[i] = char(tempChar);
  }
  return outputString;
}

bool checkUserPass(String User, String Pass) {
  String CorrectUserName = ";o}9C";
  String CorrectPassWord = "?T79!X";
  String userHash = simpleHash(User);
  String passHash = simpleHash(Pass);
  if (userHash == CorrectUserName) {
    if (passHash == CorrectPassWord) {
      return true;
    }
  }
  return false;
}

void setup()
{
  // Start the serial connection
  Serial.begin(9600);
  // start the Ethernet connection and the server:
  Ethernet.begin(mac, ip);
  server.begin();
  
  // Print the server address on serial port
  Serial.print("Server is at ");
  Serial.println(Ethernet.localIP());
  
  // Digital output setup
  pinMode(relayPin, OUTPUT);
  digitalWrite(relayPin, HIGH);
}

void loop()
{
  // listen for incoming clients
  EthernetClient client = server.available();
  if (client) {
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    boolean firstLineComplete = false;
    int contLength = 0;
    String userName = "Véronique";
    String userPass = "r00tm3";
    String requestStr = "";
    String postData = "";
    String getRequest = "";
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        requestStr.concat(c);
        Serial.write(c);
        // if you've gotten to the end of the line (received a newline
        // character) and the line is blank, the http request has ended,
        // so you can send a reply
        if (c == '\n' && currentLineIsBlank) {
          // if you've gotten a POST request, you should proceed it
          if (getRequest.startsWith("POST /") ) {
            String lengthStr = requestStr.substring(requestStr.indexOf("Content-Length:") +15);
            lengthStr.trim();
            contLength = lengthStr.toInt();
            while (contLength-- > 0) {
              c = client.read();
              postData.concat(c);
            }
            // Serial.println(postData);
            userName = postData.substring(postData.indexOf("_user_=") +7,postData.indexOf("&_pass"));
            userPass = postData.substring(postData.indexOf("&_password_=") +12);
          }
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html; charset=UTF-8");
          client.println();
          client.println("<!DOCTYPE HTML>");
          client.println("<html>");
          client.println("<head>");
          client.println("<title>Arduino ubi01</title>");
          client.println("</head>");
          client.println("<body>");
          client.println("<p>Bonjour "+userName+" !</p>");

          // Check User/pass
          if (checkUserPass(userName,userPass)) {
            // Specifics actions depending on the request
            if (getRequest.substring(getRequest.indexOf("POST /")+5,getRequest.indexOf("HTTP/")-1)=="/switchon") {
              digitalWrite(relayPin,LOW);
            }
            if (getRequest.substring(getRequest.indexOf("POST /")+5,getRequest.indexOf("HTTP/")-1)=="/switchoff") {
              digitalWrite(relayPin,HIGH);
            }
            if (getRequest.substring(getRequest.indexOf("POST /")+5,getRequest.indexOf("HTTP/")-1)!="/status") {
            }
            client.print("<p>La lampe est ");
            if ( analogRead(1) < 20 ) { client.println("allumée !</p>"); }
            if ( analogRead(1) > 1000 ) { client.println("éteinte !</p>"); }
          }
          else {
            client.println("<p>Mauvais User/Password !</p>");
          }

          // analog1 is digital7
          client.println("<form method=\"post\">");
          client.println("<p>User:<br><input type=\"text\" name=\"_user_\"><br></p>");
          client.println("<p>Password:<br><input type=\"password\" name=\"_password_\" ><br></p>");
          client.println("<p><input type=\"submit\" value=\"Allumer\" formaction=\"./switchon\">");
          client.println("<input type=\"submit\" value=\"Éteindre\" formaction=\"./switchoff\">");
          client.println("<input type=\"submit\" value=\"État\" formaction=\"./status\"></p>");
          client.println("</form>");
          client.println("</body>");
          client.println("</html>");
          break;
        }
        if (c == '\n') {
          // you're starting a new line
          currentLineIsBlank = true;
          firstLineComplete = true;
        } 
        else if (c != '\r') {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
          // Read client request, first line
          if (firstLineComplete == false) {
            getRequest.concat(c);
          }
        }
      }
    }
    // give the web browser time to receive the data
    delay(1);
    // close the connection:
    client.stop();
  }
}

//*************************************************
//  Now the functions

// String outputString ParseClientRequest(String inputString)
// {
  // outputString = inputString.substring(4);
  
// }

