

all: publish

build-html:
	for file in *.txt ; do pandoc -s $$file -o $$(echo $$(basename $$file txt)html) ; done

publish: build-html

upload:
	$(MAKE) -C arduino $@

compile:
	$(MAKE) -C arduino

clean:
	rm -f *.html

.PHONY: clean publish build-html
